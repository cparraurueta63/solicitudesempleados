import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import PropTypes from "prop-types";

import { crearEmpleado } from "../../service/empleado";
import { useState } from "react";

const FormularioEmpleado = ({ mostrarModal, cerrarModal, recargar }) => {
  const [nombre, setNombre] = useState("");
  const [salario, setSalario] = useState("");
  const [fechaIngreso, setFechaIngreso] = useState("");

  const guardarEmpleado = async (e) => {
    e.preventDefault();
    const resultado = await crearEmpleado(fechaIngreso, nombre, salario);
    if (resultado.hasErrors) {
      alert('ERROR');
    } else {
      alert('OK');
      cerrarModal();
      recargar();
    }
  };

  return (
    <>
      {mostrarModal ? (
        <Modal show={mostrarModal} onHide={cerrarModal}>
          <Modal.Header closeButton>
            <Modal.Title>Agregar Empleado</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={guardarEmpleado}>
              <Form.Group className="mb-3" controlId="nombre">
                <Form.Label>Nombre</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Camilo Parra"
                  required
                  onChange={(e) => setNombre(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="salario">
                <Form.Label>Salario</Form.Label>
                <Form.Control
                  type="number"
                  rows={3}
                  required
                  onChange={(e) => setSalario(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="fecha-ingreso">
                <Form.Label>Fecha de ingreso</Form.Label>
                <Form.Control
                  type="date"
                  rows={3}
                  format="aaaa-mm-dd"
                  required
                  onChange={(e) => setFechaIngreso(e.target.value)}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
              Guardar empleado
            </Button>
            </Form>
          </Modal.Body>
        </Modal>
      ) : null}
    </>
  );
};

FormularioEmpleado.propTypes = {
  mostrarModal: PropTypes.bool.isRequired,
  cerrarModal: PropTypes.func.isRequired,
  recargar: PropTypes.func.isRequired
};

export default FormularioEmpleado;
