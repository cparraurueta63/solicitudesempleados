import Table from "react-bootstrap/Table";
import PropTypes from "prop-types";

const TablaEmpleado = ({ empleados }) => {
  return (
    <>
    {
      empleados.length === 0
      ? <p>No hay empleados registrados</p>
      : <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Salario</th>
            <th>Fecha de Ingreso</th>
          </tr>
        </thead>
        <tbody>
          {empleados.map((empleado, i) => (
            <tr key={i}>
              <td>{empleado.ID}</td>
              <td>{empleado.NOMBRE}</td>
              <td>{empleado.SALARIO}</td>
              <td>{empleado.FECHA_INGRESO}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    }
    </>
  );
};

TablaEmpleado.propTypes = {
  empleados: PropTypes.array.isRequired,
};

export default TablaEmpleado;
