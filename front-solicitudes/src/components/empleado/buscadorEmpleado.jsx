import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useState } from "react";
import { buscarEmpleadoNombre } from "../../service/empleado";
import PropTypes from "prop-types";

const BuscadorEmpleado = ({ actualizarTabla }) => {
  const [nombre, setNombre] = useState();

  const buscarEmpleadoPorNombre = async (e) => {
    e.preventDefault();
    const respuesta = await buscarEmpleadoNombre(nombre);
    actualizarTabla(respuesta);
  };

  return (
    <Form inline>
      <Row>
        <Col xs="auto">
          <Form.Control
            type="text"
            placeholder="Search"
            className=" mr-sm-2"
            onChange={(e) => setNombre(e.target.value)}
          />
        </Col>
        <Col xs="auto">
          <Button onClick={buscarEmpleadoPorNombre} type="submit">
            Buscar
          </Button>
        </Col>
      </Row>
    </Form>
  );
};

BuscadorEmpleado.propTypes = {
  actualizarTabla: PropTypes.func.isRequired,
};

export default BuscadorEmpleado;
