import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';

const Header = () => {
  return (
    <header>
      <Navbar data-bs-theme="light">
        <Container>
          <Navbar.Brand href="/">Gestion App</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/empleados">Empleados</Nav.Link>
            <Nav.Link href="/solicitudes">Solicitudes</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </header>
  )
}

export default Header