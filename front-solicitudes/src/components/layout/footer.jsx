const Footer = () => {
  return (
    <footer className="footer">
        <p>All Rights Reserved. &copy; 2024 Camilo Parra</p>
    </footer>
  )
}

export default Footer