import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { consultarEmpleado } from "../../service/empleado";
import { crearSolicitud } from "../../service/solicitud";

const FormularioSolicitud = ({ mostrarModal, cerrarModal, recargar }) => {

  const [empleados, setEmpleados] = useState([]);
  const [codigo, setCodigo] = useState("");
  const [descripcion, setDescripcion] = useState("");
  const [resumen, setResumen] = useState("");
  const [empleado, setEmpleado] = useState("");

  useEffect(() => {
    const cargarEmpleados = async () => {
      const respuesta = await consultarEmpleado();
      setEmpleados(respuesta);
      console.log(respuesta)
    };
    cargarEmpleados();
  }, []);

  const guardarSolicitud = async (e) => {
    e.preventDefault();
    const resultado = await crearSolicitud(codigo, descripcion, resumen, empleado);
    if (resultado.hasErrors) {
      alert('ERROR');
    } else {
      alert('OK');
      cerrarModal();
      recargar();
    }
  };

  return (
    <>
      {mostrarModal ? (
        <Modal show={mostrarModal} onHide={cerrarModal}>
          <Modal.Header closeButton>
            <Modal.Title>Agregar Solicitud</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={guardarSolicitud}>
              <Form.Group className="mb-3" controlId="codigo">
                <Form.Label>Código</Form.Label>
                <Form.Control type="text" placeholder="Camilo Parra" onChange={(e) => setCodigo(e.target.value)}/>
              </Form.Group>
              <Form.Group className="mb-3" controlId="resumen">
                <Form.Label>Descripción</Form.Label>
                <Form.Control as="textarea" rows={3} onChange={(e) => setDescripcion(e.target.value)}/>
              </Form.Group>
              <Form.Group className="mb-3" controlId="resumen">
                <Form.Label>Resumen</Form.Label>
                <Form.Control as="textarea" rows={3} onChange={(e) => setResumen(e.target.value)}/>
              </Form.Group>
              <Form.Group className="mb-3" controlId="empleado">
                <Form.Label>Empleado</Form.Label>
                <Form.Select aria-label="Selecciona un empleado" onChange={(e) => setEmpleado(e.target.value)}>
                  <option>Selecciona el empleado</option>
                  {
                    empleados.map((empleado, i) => (
                      <option key={i} value={empleado.ID}>{empleado.NOMBRE}</option>
                    ))
                  }
                </Form.Select>
              </Form.Group>
              <Button variant="primary" type="submit">
              Guardar Solicitud
            </Button>
            </Form>
          </Modal.Body>
        </Modal>
      ) : null}
    </>
  );
};

FormularioSolicitud.propTypes = {
  mostrarModal: PropTypes.bool.isRequired,
  cerrarModal: PropTypes.func.isRequired,
  recargar: PropTypes.func.isRequired,
};

export default FormularioSolicitud;
