import Table from "react-bootstrap/Table";
import PropTypes from "prop-types";
import { Button } from "react-bootstrap";
import { eliminarSolicitud } from "../../service/solicitud";


const TablaSolicitud = ({solicitudes, recargar}) => {

  const eliminarSolicitudes = async (id) => {
    const resultado = await eliminarSolicitud(id);
    if (resultado.hasErrors) {
      alert('ERROR');
    } else {
      recargar();
      alert(resultado.mensaje);
    }
  }

  const handleEliminarClick = async (id) => {
    eliminarSolicitudes(id);
  }

  return (
    <>
      {
        solicitudes.length === 0
        ? <p>No hay solicitudes registradas </p>
        : <Table striped bordered hover>
            <thead>
                <tr>
                  <th>ID</th>
                  <th>Código</th>
                  <th>Descripción</th>
                  <th>Resumen</th>
                  <th>Empleado</th>
                  <th>Opción</th>
                </tr>
              </thead>
              <tbody>
              {
                solicitudes.map((solicitud, i) => (
                  <tr key={i}>
                    <td>{solicitud.ID}</td>
                    <td>{solicitud.CODIGO}</td>
                    <td>{solicitud.DESCRIPCION}</td>
                    <td>{solicitud.RESUMEN}</td>
                    <td>{solicitud.empleado?.NOMBRE}</td>
                    <td>
                      <Button 
                        onClick={(e) => {
                          e.preventDefault();
                          handleEliminarClick(solicitud.ID);
                        }}
                        disabled={solicitud.eliminando}
                      >
                        {solicitud.eliminando ? 'Eliminando...' : 'Eliminar'}
                      </Button>
                    </td>
                </tr>
                ))
              }
              </tbody>
          </Table>
      }
    </>
  );
};

TablaSolicitud.propTypes = {
  solicitudes: PropTypes.array.isRequired,
  recargar: PropTypes.func.isRequired,
};


export default TablaSolicitud;
