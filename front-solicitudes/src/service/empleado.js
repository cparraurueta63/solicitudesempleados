import axios from "axios";
const port = 8005;

const BASE_URL = "http://localhost:" + port;

export const consultarEmpleado = async () => {
  const result = await axios.get(BASE_URL + "/empleados");
  return result.data;
};
export const buscarEmpleadoNombre = async (nombre) => {
  const result = await axios.get(
    BASE_URL + "/empleados/filtro?nombre=" + nombre
  );
  return result.data;
};

export const crearEmpleado = async (fecha_ingreso, nombre, salario) => {
  const empleado = {
    "fecha_ingreso": fecha_ingreso,
    "nombre": nombre,
    "salario": salario,
  };

  try {
    const result = await axios.post(BASE_URL + "/empleados", empleado) ;
    return result.data;
  } catch (error) {
    if (error.response && error.response.status === 400) {
      return error.response.data;
    }
  }
};
