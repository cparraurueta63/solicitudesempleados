
import axios from 'axios';
const port = 8005;
const BASE_URL = "http://localhost:" + port;

export const consultarSolicitudes = async () => {
    const result = await axios.get( BASE_URL +'/solicitudes');  
    return result.data;
}

export const crearSolicitud = async (codigo, descripcion, resumen, idEmpleado) => {
    const solicitud = {
        "codigo": codigo,
        "descripcion": descripcion,
        "resumen": resumen,
        "id_empleado": idEmpleado,
    };
  
    try {
      const result = await axios.post(BASE_URL + "/solicitudes", solicitud) ;
      return result.data;
    } catch (error) {
      if (error.response && error.response.status === 400) {
        return error.response.data;
      }
    }
  };
  
  export const eliminarSolicitud = async (id) => {
    const result = await axios.delete(BASE_URL+`/solicitudes/${id}`);
    return result.data;
}
