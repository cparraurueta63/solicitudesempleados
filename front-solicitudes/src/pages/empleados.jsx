import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Footer from "../components/layout/footer";
import Header from "../components/layout/header";
import BuscadorEmpleado from "../components/empleado/buscadorEmpleado";
import TablaEmpleado from "../components/empleado/tablaEmpleado";
import { useEffect, useState } from "react";
import FormularioEmpleado from "../components/empleado/formularioEmpleado";

import { consultarEmpleado } from "../service/empleado";

const Empleados = () => {
  const [mostrarModal, setMostrarModal] = useState(false);
  const [empleados, setEmpleados] = useState([]);
  const [recargar, setRecargar] = useState(false);

  const abrirModal = () => {
    setMostrarModal(true);
  };

  const cerrarModal = () => {
    setMostrarModal(false);
  };

  const actualizarTabla = (empleados) => {
    setEmpleados(empleados);
  };

  const recargarFunction = () => {
    setRecargar(!recargar);
  }

  useEffect(() => {
    const cargarEmpleados = async () => {
      const respuesta = await consultarEmpleado();
      setEmpleados(respuesta);
    };
    cargarEmpleados();
  }, []);

  return (
    <>
      <Header />
      <div className="contenedor-cliente">
        <Container>
          <Row className="justify-content-md-center mb-3">
            <Col lg={9}>
              <BuscadorEmpleado actualizarTabla={actualizarTabla} />
            </Col>
            <Col className="d-flex justify-content-end" lg={3}>
              <Button variant="primary" onClick={abrirModal}>
                Agregar Empleado
              </Button>
            </Col>
          </Row>
          <Row>
            <Col>
              <TablaEmpleado empleados={empleados} />
            </Col>
          </Row>
        </Container>
      </div>
      <FormularioEmpleado
        mostrarModal={mostrarModal}
        cerrarModal={cerrarModal}
        recargar={recargarFunction}
      ></FormularioEmpleado>
      <Footer />
    </>
  );
};

export default Empleados;
