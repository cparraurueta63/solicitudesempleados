import Container from "react-bootstrap/esm/Container"
import Footer from "../components/layout/footer"
import Header from "../components/layout/header"
import Row from "react-bootstrap/esm/Row"
import Col from "react-bootstrap/esm/Col"
import Button from "react-bootstrap/esm/Button"
import TablaSolicitudes from "../components/solicitudes/tablaSolicitudes"
import { useEffect, useState } from "react"
import FormularioSolicitud from "../components/solicitudes/formularioSolicitud"

import { consultarSolicitudes } from "../service/solicitud";


const Solicitudes = () => {

    const [mostrarModal, setMostrarModal] = useState(false);
    const [solicitudes, setSolicitudes] = useState([]);
    const [recargar, setRecargar] = useState(false)

    const abrirModal = () => {
      setMostrarModal(true);
    };
  
    const cerrarModal = () => {
      setMostrarModal(false);
    };

    const recargarFunction = () => {
      setRecargar(!recargar);
    }

    useEffect(() => {
      const cargarSolicitudes = async () => {
        const respuesta = await consultarSolicitudes();
        setSolicitudes(respuesta);
      };
      cargarSolicitudes();
    }, [recargar]);
  

  return (
    <>
      <Header/>
      <div className="contenedor-solicitudes">
      <Container>
            <Row className="justify-content-md-center mb-3">
                <Col className="d-flex justify-content-end" lg={12} ><Button variant="primary" onClick={abrirModal}>Agregar Solicitud</Button></Col>
            </Row>
            <Row>
              <Col>
               <TablaSolicitudes solicitudes={solicitudes} recargar={recargarFunction}/>
              </Col>
            </Row>
        </Container>
      </div>
        <FormularioSolicitud mostrarModal={mostrarModal} cerrarModal={cerrarModal} recargar={recargarFunction}></FormularioSolicitud>
      <Footer/>
    </>
  )
}

export default Solicitudes
