// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
// import { Link } from 'react-router-dom'
// import './App.css'
import Footer from './components/layout/footer'
import Header from './components/layout/header'

const App = () => {
  // const [count, setCount] = useState(0)

  return (
    <>
      <Header/>
      <Footer/>
    </>
  )
}

export default App