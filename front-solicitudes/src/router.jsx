import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from './App';
import Solicitudes from './pages/solicitudes';
import Empleados from "./pages/empleados";
import NotFound from "./pages/NotFound";

const Router = () => {
  return (
    <BrowserRouter>
        <Routes>
            <Route exact path="/" element={<App/>} />
            <Route exact path='/empleados' element={<Empleados/>}/>
            <Route exact path='/solicitudes' element={<Solicitudes/>}/> 
            <Route exact path='*' element={<NotFound/>}/> 
        </Routes>
    </BrowserRouter>
  );
};

export default Router;