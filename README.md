# Gestion de solicitudes

Este proyecto cuenta con dos carpetas, una para el back y otra para el front. 

Con las siguientes Caracteristicas

- Consultar e insertar información de la tabla empleado.
- Consultar empleados por nombre.
- Consultar, eliminar e insertar información de la tabla Solicitud.
- Cuando se visualicen los datos de la tabla solicitud se debe mostrar el nombre del empleado y no el id_empleado.

## Caracteristica Back 
El back fue realizado con Node JS, cuenta con dos tablas una para empleados y otras para solicitudes 

### Dependencias  back
- cors: ^2.8.5
- express: ^4.18.3
- mysql2: ^3.9.2
- sequelize: ^6.37.1

### Intalacion Back 

- Navega hasta la carpeta back-solicitudes en tu terminal.
- Ejecuta el comando npm install  
- Creamos la base de datos con el siguiente SQL o en la ruta back-solicitudes/src/database/dabase.js tenemos el SQL:


sql
CREATE DATABASE IF NOT EXISTS solicitudes_empleado;
USE solicitudes_empleado;

CREATE TABLE empleado (
  ID int NOT NULL AUTO_INCREMENT,
  FECHA_INGRESO date DEFAULT NULL,
  NOMBRE varchar(50) DEFAULT NULL,
  SALARIO float DEFAULT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO empleado (FECHA_INGRESO, NOMBRE, SALARIO) VALUES
  ('2024-03-14', 'CAMILO ANDRES PARRA URUETA', 3100000),
  ('2024-03-14', 'JENIFER MEDINA', 3100000),
  ('2024-03-15', 'PETRONA URUEA', 3100000);

CREATE TABLE solicitud (
  ID int NOT NULL AUTO_INCREMENT,
  CODIGO varchar(50) DEFAULT NULL,
  DESCRIPCION varchar(50) DEFAULT NULL,
  RESUMEN varchar(50) DEFAULT NULL,
  ID_EMPLEADO int DEFAULT NULL,
  PRIMARY KEY (ID),
  CONSTRAINT FK_solicitud_empleado FOREIGN KEY (ID_EMPLEADO) REFERENCES empleado (ID) ON DELETE CASCADE ON UPDATE CASCADE
);

- Configuramos la conexion en la ruta back-solicitudes/src/database/conexion.js
- Para ejecutar el proyecto nos vamos a la raiz de la carperta y ejecutamos node index.js

## Caracteristica Front 
El front fue realizado con React y Vite para un desarrollo rápido y eficiente. 

### Dependencias  Front
- axios: ^1.6.7
- bootstrap: ^5.3.3
- react: ^18.2.0
- react-bootstrap: ^2.10.1
- react-dom: ^18.2.0
- react-router-dom: ^6.22.3

### Intalacion Front 
- Navega hasta la carpeta front-solicitudes en tu terminal.
- Ejecuta el comando npm install y Luego para ponerlo en marcha ejecuta npm run dev
