const empleadoService = require("../service/empleadoService");

const empleados = async (request, response, next) => {
  try {
    const empleados = await empleadoService.mostrarEmpleados();
    response.status(200).json(empleados);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearEmpleado = async (request, response, next) => {
  try {
    const empleado = await empleadoService.crearEmpleado(request.body);
    response
      .status(200)
      .json({ empleado, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const buscarEmpleado = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const empleadoFiltrado = await empleadoService.buscarEmpleado(nombre);
    response.status(200).json(empleadoFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarEmpleado = async (request, response, next) => {
  try {
    const id  = request.params.id;
    const empleado = await empleadoService.editarEmpleado(id, request.body);
    response.status(200).json(empleado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

module.exports = {
  empleados,
  crearEmpleado,
  buscarEmpleado,
  editarEmpleado,
};
