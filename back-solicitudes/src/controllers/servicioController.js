const servicioService = require("../service/servicioService");

const obtenerServicios = async (request, response, next) => {
  try {
    const servicios = await servicioService.mostrarServicios();
    response.status(200).json(servicios);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearServicio = async (request, response, next) => {
  try {
    const servicio = await servicioService.crearServicio(request.body);
    response
      .status(200)
      .json({ servicio, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const buscarServicio = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const servicioFiltrado = await servicioService.buscarServicio(nombre);
    response.status(200).json(servicioFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarServicio = async (request, response, next) => {
  try {
    const { id } = request.params;
    const servicio = await servicioService.editarServicio(id, request.body);
    response.status(200).json(servicio);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const eliminarServicio = async (request, response, next) => {
  try {
    const { id } = request.params;

    const eliminado = await servicioService.borradoLogico(id);
    response.status(200).json(eliminado);    
  } catch (error) {
    response.status(500).json("Error al eliminar el servicio");
  }
};

module.exports = {
  obtenerServicios,
  crearServicio,
  buscarServicio,
  editarServicio,
  eliminarServicio,
};
