const clienteService = require("../service/clienteService");

const obtenerClientes = async (request, response, next) => {
  try {
    const clientes = await clienteService.mostrarClientes();
    response.status(200).json(clientes);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearCliente = async (request, response, next) => {
  try {
    const cliente = await clienteService.crearCliente(request.body);
    response
      .status(200)
      .json({ cliente, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};




const buscarCliente = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const clienteFiltrado = await clienteService.buscarCliente(nombre);
    response.status(200).json(clienteFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarCliente = async (request, response, next) => {
  try {
    const { id } = request.params;
    const cliente = await clienteService.editarCliente(id, request.body);
    response.status(200).json(cliente);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const eliminarCliente = async (request, response, next) => {
  try {
    const { id } = request.params;

    const eliminado = await clienteService.borradoLogico(id);
    response.status(200).json(eliminado);    
  } catch (error) {
    response.status(500).json("Error al eliminar el cliente");
  }
};

module.exports = {
  obtenerClientes,
  crearCliente,
  buscarCliente,
  editarCliente,
  eliminarCliente,
};
