const ingresoService = require("../service/ingresosService");

const obteneringresos = async (request, response, next) => {
  try {
    const ingreso = await ingresoService.mostrarIngreso();
    response.status(200).json(ingreso);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const obteneringresosCategorias = async (request, response, next) => {
  try {
    const categorias = await ingresoService.mostrarCategoriaIngreso();
    response.status(200).json(categorias);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearIngreso = async (request, response, next) => {
  try {
    const ingreso = await ingresoService.crearIngreso(request.body);
    response
      .status(200)
      .json({ ingreso, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const buscarIngreso = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const ingresoFiltrado = await ingresoService.buscarIngreso(nombre);
    response.status(200).json(ingresoFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarIngreso = async (request, response, next) => {
  try {
    const id  = request.params.id;
    const ingreso = await ingresoService.editarIngreso(id, request.body);
    response.status(200).json(ingreso);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};


const eliminarIngreso = async (request, response, next) => {
    try {
      const { id } = request.params;
  
      const eliminado = await ingresoService.borradoLogico(id);
      response.status(200).json(eliminado);    
    } catch (error) {
      response.status(500).json("Error al eliminar el cliente");
    }
  };

module.exports = {
    obteneringresos,
    obteneringresosCategorias,
    crearIngreso,
    buscarIngreso,
    editarIngreso,
    eliminarIngreso

};
