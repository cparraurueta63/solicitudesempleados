const gastoService = require("../service/gastosService");

const obtenerGastos = async (request, response, next) => {
  try {
    const gasto = await gastoService.mostrarGasto();
    response.status(200).json(gasto);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const obtenergastosCategorias = async (request, response, next) => {
  try {
    const categorias = await gastoService.mostrarCategoriaGasto();
    response.status(200).json(categorias);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearGasto = async (request, response, next) => {
  try {
    const gasto = await gastoService.crearGasto(request.body);
    response
      .status(200)
      .json({ gasto, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const buscarGasto = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const gastoFiltrado = await gastoService.buscarGasto(nombre);
    response.status(200).json(gastoFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarGasto = async (request, response, next) => {
  try {
    const id  = request.params.id;
    const gasto = await gastoService.editarGasto(id, request.body);
    response.status(200).json(gasto);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};


const eliminarGasto = async (request, response, next) => {
    try {
      const { id } = request.params;
  
      const eliminado = await gastoService.borradoLogico(id);
      response.status(200).json(eliminado);    
    } catch (error) {
      response.status(500).json("Error al eliminar el cliente");
    }
  };

module.exports = {
    obtenerGastos,
    obtenergastosCategorias,
    crearGasto,
    buscarGasto,
    editarGasto,
    eliminarGasto

};
