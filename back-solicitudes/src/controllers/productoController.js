const productoService = require("../service/productoService");

const obtenerproductos = async (request, response, next) => {
  try {
    const producto = await productoService.mostrarProducto();
    response.status(200).json(producto);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const obtenerproductosCategorias = async (request, response, next) => {
  try {
    const categorias = await productoService.mostrarCategoriaProducto();
    response.status(200).json(categorias);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const crearProducto = async (request, response, next) => {
  try {
    const producto = await productoService.crearProducto(request.body);
    response
      .status(200)
      .json({ producto, mensaje: "Se ha guardado correctamente" });
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const buscarProducto = async (request, response, next) => {
  try {
    const nombre = request.query.nombre;
    const productoFiltrado = await productoService.buscarProducto(nombre);
    response.status(200).json(productoFiltrado);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};

const editarProducto = async (request, response, next) => {
  try {
    const id  = request.params.id;
    const producto = await productoService.editarProducto(id, request.body);
    response.status(200).json(producto);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};


const eliminarProducto = async (request, response, next) => {
    try {
      const { id } = request.params;
  
      const eliminado = await productoService.borradoLogico(id);
      response.status(200).json(eliminado);    
    } catch (error) {
      response.status(500).json("Error al eliminar el cliente");
    }
  };

module.exports = {
    obtenerproductos,
    obtenerproductosCategorias,
    crearProducto,
    buscarProducto,
    editarProducto,
    eliminarProducto

};
