const solicitudesService = require("../service/solicitudService");

const solicitudes = async (request, response, next) => {
  try {
    const solicitudes = await solicitudesService.mostrarSolicitudes();
    response.status(200).json(solicitudes);
  } catch (error) {
    response.status(500).json("ha ocurrido un error: " + error);
  }
};


const crearSolicitud = async (request, response, next) => {
    try {
      // const empleadoReq = {
      //     fecha_ingreso: request.body.fecha_ingreso,
      // }
      const solicitud = await solicitudesService.crearSolicitud(request.body);
      response
        .status(200)
        .json({ solicitud, mensaje: "Se ha guardado correctamente" });
    } catch (error) {
      response.status(500).json("ha ocurrido un error: " + error);
    }
  };

  const eliminarSolicitud = async (request, response, next) => {
    try {
      const id = request.params.id; // Obtener el ID de la solicitud de los parámetros de la solicitud
      await solicitudesService.eliminarSolicitud(id);
      response.status(200).json({ mensaje: "Solicitud eliminada correctamente" });
    } catch (error) {
      response.status(404).json("Ha ocurrido un error: " + error);
    }
  };





module.exports = {
  solicitudes,
  crearSolicitud,
  eliminarSolicitud
};
