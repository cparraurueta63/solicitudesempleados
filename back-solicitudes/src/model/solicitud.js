const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");
const Empleado = require("./empleado");

const Solicitudes = sequelize.define(
  "Solicitudes",
  {
    ID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    CODIGO: {
        type: DataTypes.STRING(50),
    },
    DESCRIPCION: {
      type: DataTypes.STRING(50),
    },
    RESUMEN: {
      type: DataTypes.STRING(50),
    },
    ID_EMPLEADO: {
      type: DataTypes.INTEGER,
      references: {
        model: Empleado, 
        key: 'ID', 
       
      }
    },
  },
  {
    tableName: "solicitud",
    timestamps: false,
  }
);

Solicitudes.belongsTo(Empleado, { foreignKey: 'ID_EMPLEADO', as: 'empleado' });


module.exports = Solicitudes;
