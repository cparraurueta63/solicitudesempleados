const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");

const Cliente = sequelize.define(
  "Cliente",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  
    nombre: {
      type: DataTypes.STRING(255),
    },
    cedula: {
      type: DataTypes.STRING(20),
    },
    celular: {
      type: DataTypes.STRING(20),
    },
    correo: {
      type: DataTypes.STRING(255),
    },
    direccion: {
      type: DataTypes.STRING(255),
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    borrado_logico: {
        type: DataTypes.INTEGER,
        defaultValue: 0, 
      },
  
  },
  {
    tableName: "clientes",
    timestamps: false,
  }
);



module.exports = Cliente;
