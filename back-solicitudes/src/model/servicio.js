const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");

const Servicio = sequelize.define(
  "servicio",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },

    nombre: {
      type: DataTypes.STRING(255),
    },
    valor_1: {
      type: DataTypes.DECIMAL(10, 2),
    },
    valor_2: {
      type: DataTypes.DECIMAL(10, 2),
    },
    valor_3: {
      type: DataTypes.DECIMAL(10, 2),
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
    },
    borrado_logico: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  },
  {
    tableName: "servicios",
    timestamps: false,
  }
);

module.exports = Servicio;
