const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");
const CategoriaProducto = require("./categorias/categoriaProducto");


const Producto = sequelize.define(
  "Producto",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    nombre: {
      type: DataTypes.STRING(255),
    },
    stock: {
      type: DataTypes.INTEGER,
    },
    valor_venta_1: {
      type: DataTypes.DECIMAL(10, 2),
    },
    valor_venta_2: {
      type: DataTypes.DECIMAL(10, 2),
    },
    valor_venta_3: {
      type: DataTypes.DECIMAL(10, 2),
    },
    costo: {
      type: DataTypes.DECIMAL(10, 2),
    },
    categoria_id: {
      type: DataTypes.INTEGER,
      references: {
        model: CategoriaProducto,
        key: "id",
      },
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
    },
    borrado_logico: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  },
  {
    tableName: "productos",
    timestamps: false,
  }
);


Producto.belongsTo(CategoriaProducto, { foreignKey: 'categoria_id', as: 'categoria_productos' })

module.exports = Producto;
