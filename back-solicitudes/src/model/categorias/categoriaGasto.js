const { DataTypes } = require("sequelize");
const sequelize = require("../../database/conexion");

const CategoriaGasto = sequelize.define(
  "categoriaGastos",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  
    nombre: {
      type: DataTypes.STRING(50),
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    borrado_logico: {
        type: DataTypes.INTEGER,
        defaultValue: 0, 
      },
  },
  {
    tableName: "categoria_gastos",
    timestamps: false,
  }
);

module.exports = CategoriaGasto;
