const { DataTypes } = require("sequelize");
const sequelize = require("../../database/conexion");

const CategoriaIngreso = sequelize.define(
  "categoriaIngresos",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  
    nombre: {
      type: DataTypes.STRING(50),
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    borrado_logico: {
        type: DataTypes.INTEGER,
        defaultValue: 0, 
      },
  },
  {
    tableName: "categoria_ingresos",
    timestamps: false,
  }
);

module.exports = CategoriaIngreso;
