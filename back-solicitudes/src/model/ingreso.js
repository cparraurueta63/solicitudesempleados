const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");
const CategoriaIngresos = require("./categorias/categoriaIngreso");


const Ingreso = sequelize.define(
  "Ingreso",
  {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    nombre: {
      type: DataTypes.STRING(255),
    },
    descripcion: {
      type: DataTypes.STRING(255),
    },
    
    valor: {
      type: DataTypes.DECIMAL(10, 2),
    },
    categoria_id: {
      type: DataTypes.INTEGER,
      references: {
        model: CategoriaIngresos,
        key: "id",
      },
    },
    fecha_creacion: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: sequelize.literal("CURRENT_TIMESTAMP"),
    },
    borrado_logico: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
  },
  {
    tableName: "ingresos",
    timestamps: false,
  }
);


Ingreso.belongsTo(CategoriaIngresos, { foreignKey: 'categoria_id', as: 'categoria_ingresos' })

module.exports = Ingreso;
