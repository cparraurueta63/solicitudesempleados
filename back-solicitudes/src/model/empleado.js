const { DataTypes } = require("sequelize");
const sequelize = require("../database/conexion");

const Empleado = sequelize.define(
  "Empleado",
  {
    ID: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    FECHA_INGRESO: {
      type: DataTypes.DATE,
    },
    NOMBRE: {
      type: DataTypes.STRING(50),
    },
    SALARIO: {
      type: DataTypes.NUMBER,
    },
  },
  {
    tableName: "empleado",
    timestamps: false,
  }
);

module.exports = Empleado;
