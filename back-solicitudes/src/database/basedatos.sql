CREATE DATABASE IF NOT EXISTS solicitudes_empleado;
USE solicitudes_empleado;

CREATE TABLE empleado (
  ID int NOT NULL AUTO_INCREMENT,
  FECHA_INGRESO date DEFAULT NULL,
  NOMBRE varchar(50) DEFAULT NULL,
  SALARIO float DEFAULT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO empleado (FECHA_INGRESO, NOMBRE, SALARIO) VALUES
  ('2024-03-14', 'CAMILO ANDRES PARRA URUETA', 3100000),
  ('2024-03-14', 'JENIFER MEDINA', 3100000),
  ('2024-03-15', 'PETRONA URUEA', 3100000);

CREATE TABLE solicitud (
  ID int NOT NULL AUTO_INCREMENT,
  CODIGO varchar(50) DEFAULT NULL,
  DESCRIPCION varchar(50) DEFAULT NULL,
  RESUMEN varchar(50) DEFAULT NULL,
  ID_EMPLEADO int DEFAULT NULL,
  PRIMARY KEY (ID),
  CONSTRAINT FK_solicitud_empleado FOREIGN KEY (ID_EMPLEADO) REFERENCES empleado (ID) ON DELETE CASCADE ON UPDATE CASCADE
);


-- Crear tabla de categorías de productos
CREATE TABLE categoria_productos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de productos
CREATE TABLE productos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    stock INT,
    valor_venta_1 DECIMAL(10, 2),
    valor_venta_2 DECIMAL(10, 2),
    valor_venta_3 DECIMAL(10, 2),
    costo DECIMAL(10, 2),
    categoria_id INT,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (categoria_id) REFERENCES categoria_productos(id)
);

-- Crear tabla de servicios
CREATE TABLE servicios (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    valor_1 DECIMAL(10, 2),
    valor_2 DECIMAL(10, 2),
    valor_3 DECIMAL(10, 2),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de categorías de gastos
CREATE TABLE categoria_gastos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de gastos
CREATE TABLE gastos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    descripcion TEXT,
    valor DECIMAL(10, 2),
    categoria_id INT,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (categoria_id) REFERENCES categoria_gastos(id)
);

-- Crear tabla de categorías de ingresos
CREATE TABLE categoria_ingresos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de ingresos
CREATE TABLE ingresos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    descripcion TEXT,
    valor DECIMAL(10, 2),
    categoria_id INT,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (categoria_id) REFERENCES categoria_ingresos(id)
);

-- Crear tabla de clientes
CREATE TABLE clientes (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    cedula VARCHAR(20) NOT NULL,
    celular VARCHAR(20),
    correo VARCHAR(255),
    direccion VARCHAR(255),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de ventas
CREATE TABLE cabecera_venta (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_cliente INT,
    valor_venta DECIMAL(10, 2),
    saldo_pendiente DECIMAL(10, 2),
    vuelto DECIMAL(10, 2),
    id_cuenta_bancaria INT,
    recibo_dinero VARCHAR(255),
    tipo_venta VARCHAR(50),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (id_cliente) REFERENCES clientes(id),
    FOREIGN KEY (id_cuenta_bancaria) REFERENCES cuenta_bancaria(id)
);

CREATE TABLE detalles_venta (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_cabecera_venta INT,
    id_producto INT,
    id_servicio INT,
    cantidad INT,
    subtotal DECIMAL(10, 2),
    total DECIMAL(10, 2),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (id_cabecera_venta) REFERENCES cabecera_venta(id),
    FOREIGN KEY (id_producto) REFERENCES productos(id),
    FOREIGN KEY (id_servicio) REFERENCES servicios(id)
);

CREATE TABLE cuenta_bancaria (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    numero_cuenta VARCHAR(50) NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);

-- Crear tabla de cortes
CREATE TABLE cortes (
    id INT PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(255),
    ganancia DECIMAL(10, 2),
    gastos DECIMAL(10, 2),
    ingresos DECIMAL(10, 2),
    ventas DECIMAL(10, 2),
    anotacion TEXT,
    fecha_inicio DATE,
    fecha_fin DATE,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    borrado_logico BOOLEAN DEFAULT FALSE
);
