const Empleado = require("../model/empleado");
const sequelize = require("../database/conexion");

const mostrarEmpleados = async () => {
  const empleados = await Empleado.findAll();
  return empleados;
};
const crearEmpleado = async (empleado) => {
  const empleado_nuevo = await Empleado.create({
    FECHA_INGRESO: empleado.fecha_ingreso,
    NOMBRE: empleado.nombre,
    SALARIO: empleado.salario,
  });

  return empleado_nuevo;
};
const buscarEmpleado = async (nombre) => {
  const empleadoFiltrado = await sequelize.query(
    `SELECT * FROM Empleado WHERE NOMBRE LIKE '%${nombre}%'`
  );

  return empleadoFiltrado[0];
};

// const buscarEmpleadoID = async (id) => {
//   const empleado = await Empleado.findByPk(id);
//   return empleado;
// }

const editarEmpleado = async (id, paciente) => {
  const empleado_actual = await Empleado.findByPk(id);
  if (empleado_actual) {
    await empleado_actual.update({
      NOMBRE: paciente.nombre,
      SALARIO: paciente.salario,
    });
  } 

  return empleado_actual;
};

module.exports = {
  mostrarEmpleados,
  crearEmpleado,
  buscarEmpleado,
  // buscarEmpleadoID,
  editarEmpleado
};
