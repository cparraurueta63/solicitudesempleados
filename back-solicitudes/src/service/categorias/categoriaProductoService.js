const CategoriaProducto = require("../../model/categorias/categoriaProducto");
const sequelize = require("../../database/conexion");

const obtenerCategoriaProducto = async () => {
  const categoriaProducto = await CategoriaProducto.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return categoriaProducto;
}; 

module.exports = {
  obtenerCategoriaProducto,

};
