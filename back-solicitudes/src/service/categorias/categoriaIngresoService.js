const CategoriaIngreso = require("../../model/categorias/categoriaIngreso");
const sequelize = require("../../database/conexion");

const obtenerCategoriaIngreso = async () => {
  const categoriaIngresos = await CategoriaIngreso.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return categoriaIngresos;
};

module.exports = {
  obtenerCategoriaIngreso,

};
