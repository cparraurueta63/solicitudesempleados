const CategoriaGasto = require("../../model/categorias/categoriaGasto");
const sequelize = require("../../database/conexion");

const obtenerCategoriaGasto = async () => {
  const categoriaGasto = await CategoriaGasto.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return categoriaGasto;
};

module.exports = {
  obtenerCategoriaGasto,

};
