const Gasto = require("../model/gasto");
const CategoriaGasto = require("../model/categorias/categoriaGasto");
const sequelize = require("../database/conexion");

const mostrarCategoriaGasto = async () => {
  const categoriaGasto = await CategoriaGasto.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return categoriaGasto;
};

const mostrarGasto = async () => {
  const gasto = await Gasto.findAll({
    where: {
      borrado_logico: 0,
    },
    include: "categoria_gastos",
  });
  return gasto;
};

const crearGasto = async (gasto) => {
  const gasto_nuevo = await Gasto.create({
    nombre: gasto.nombre,
    descripcion: gasto.descripcion,
    valor: gasto.valor,
    categoria_id: gasto.categoria_id,
  });

  return gasto_nuevo;
};

const buscarGasto = async (nombre) => {
  const gastoFiltrado = await sequelize.query(
    `SELECT * FROM gastos WHERE nombre LIKE '%${nombre}%'`
  );

  return gastoFiltrado[0];
};

const borradoLogico = async (id) => {
  const gastoBorrado = await Gasto.findByPk(id);
  if (gastoBorrado) {
    await gastoBorrado.update({ borrado_logico: 1 });
    return { gasto: gastoBorrado, message: "Cliente eliminado correctamente" };
  }
  return null;
};

const editarGasto = async (id, gasto) => {
  const gasto_actual = await Gasto.findByPk(id);
  if (gasto_actual) {
    await gasto_actual.update({
      nombre: gasto.nombre,
      descripcion: gasto.valor,
      categoria_id: gasto.categoria_id,
    });
  }

  return { gasto: gasto_actual, message: "Gasto Editado correctamente" };
};

module.exports = {
  mostrarCategoriaGasto,
  mostrarGasto,
  crearGasto,
  buscarGasto,
  borradoLogico,
  editarGasto,
};
