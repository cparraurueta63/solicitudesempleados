const Ingreso = require("../model/ingreso");
const CategoriaIngreso = require("../model/categorias/categoriaIngreso");
const sequelize = require("../database/conexion");

const mostrarCategoriaIngreso = async () => {
    const categoriaIngreso = await CategoriaIngreso.findAll({
      where: {
        borrado_logico: 0,
      },
    });
    return categoriaIngreso;
  };
  
  
  const mostrarIngreso = async () => {
    const ingreso = await Ingreso.findAll({
      where: {
        borrado_logico: 0,
      },
      include: "categoria_ingresos"
    });
    return ingreso;
  };
  
  
  const crearIngreso = async (ingreso) => {
    const ingreso_nuevo = await Ingreso.create({
        nombre: ingreso.nombre,
        descripcion: ingreso.descripcion,
        valor: ingreso.valor,
        categoria_id: ingreso.categoria_id,
      
    });
  
    return ingreso_nuevo;
  };
  
  const buscarIngreso = async (nombre) => {
    const ingresoFiltrado = await sequelize.query(
      `SELECT * FROM ingresos WHERE nombre LIKE '%${nombre}%'`
    );
  
    return ingresoFiltrado[0];
  };
  
  const borradoLogico = async (id) => {
    const ingresoBorrado = await Ingreso.findByPk(id);
    if (ingresoBorrado) {
      await ingresoBorrado.update({ borrado_logico: 1 });
      return { ingreso: ingresoBorrado, message: "Cliente eliminado correctamente" };
    }
    return null;
  };
  
  const editarIngreso = async (id, ingreso) => {
    const ingreso_actual = await Ingreso.findByPk(id);
    if (ingreso_actual) {
      await ingreso_actual.update({
        nombre: ingreso.nombre,
        descripcion: ingreso.valor,
        categoria_id: ingreso.categoria_id,
      });
    }
  
    return { ingreso: ingreso_actual, message: "Ingreso Editado correctamente" };;
  };
  
  module.exports = {
      mostrarCategoriaIngreso,
      mostrarIngreso,
      crearIngreso,
      buscarIngreso,
      borradoLogico,
      editarIngreso
  
    
  };