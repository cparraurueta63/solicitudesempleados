const Servicio = require("../model/servicio");
const sequelize = require("../database/conexion");

const mostrarServicios = async () => {
  const servicios = await Servicio.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return servicios;
};
const crearServicio = async (servicio) => {
  const servicio_nuevo = await Servicio.create({
    nombre: servicio.nombre,
    valor_1: servicio.valor_1,
    valor_2: servicio.valor_2,
    valor_3: servicio.valor_3,
  });

  return servicio_nuevo;
};

const buscarServicio = async (nombre) => {
  const servicioFiltrado = await sequelize.query(
    `SELECT * FROM servicios WHERE nombre LIKE '%${nombre}%'`
  );

  return servicioFiltrado[0];
};

const borradoLogico = async (id) => {
  const servicioBorrado = await Servicio.findByPk(id);
  if (servicioBorrado) {
    await servicioBorrado.update({ borrado_logico: 1 });
    return {
      servicio: servicioBorrado,
      message: "Servicio eliminado correctamente",
    };
  }
  return null;
};

const editarServicio = async (id, servicio) => {
  const servicio_actual = await Servicio.findByPk(id);
  if (servicio_actual) {
    await servicio_actual.update({
      nombre: servicio.nombre,
      valor_1: servicio.valor_1,
      valor_2: servicio.valor_2,
      valor_3: servicio.valor_3,
    });
  } else {
    return { message: "No existe el servicio" };
  }

  return {
    servicio: servicio_actual,
    message: "Servicio Editado correctamente",
  };
};

module.exports = {
  mostrarServicios,
  crearServicio,
  buscarServicio,
  editarServicio,
  borradoLogico,
};
