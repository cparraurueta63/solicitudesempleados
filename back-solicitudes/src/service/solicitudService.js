const Solicitud = require("../model/solicitud");
const sequelize = require("../database/conexion");

const mostrarSolicitudes = async () => {
  const solicitudes = await Solicitud.findAll({
    include: "empleado",
  });
  return solicitudes;
};
const crearSolicitud = async (solicitud) => {
  const solicitud_nueva = await Solicitud.create({
    CODIGO: solicitud.codigo,
    DESCRIPCION: solicitud.descripcion,
    RESUMEN: solicitud.resumen,
    ID_EMPLEADO: solicitud.id_empleado,
  });

  return solicitud_nueva;
};

const eliminarSolicitud = async (id) => {
  const solicitud = await Solicitud.findByPk(id);
  if (!solicitud) {
    throw new Error(" La solicitud no fue encontrada");
  }
  await solicitud.destroy();
  return { mensaje: "Solicitud eliminada exitosamente" };
};

module.exports = {
  mostrarSolicitudes,
  crearSolicitud,
  eliminarSolicitud,
};
