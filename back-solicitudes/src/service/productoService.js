const Producto = require("../model/producto");
const CategoriaProducto = require("../model/categorias/categoriaProducto");
const sequelize = require("../database/conexion");

const mostrarCategoriaProducto = async () => {
  const categoriaProducto = await CategoriaProducto.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return categoriaProducto;
};


const mostrarProducto = async () => {
  const producto = await Producto.findAll({
    where: {
      borrado_logico: 0,
    },
    include: "categoria_productos"
  });
  return producto;
};


const crearProducto = async (producto) => {
  const producto_nuevo = await Producto.create({
    nombre: producto.nombre,
    stock: producto.cedula,
    valor_venta_1: producto.valor_venta_1,
    valor_venta_2: producto.valor_venta_2,
    valor_venta_3: producto.valor_venta_3,
    costo: producto.costo,
    categoria_id: producto.categoria_id,
    
  });

  return producto_nuevo;
};

const buscarProducto = async (nombre) => {
  const productoFiltrado = await sequelize.query(
    `SELECT * FROM productos WHERE nombre LIKE '%${nombre}%'`
  );

  return productoFiltrado[0];
};

const borradoLogico = async (id) => {
  const productoBorrado = await Producto.findByPk(id);
  if (productoBorrado) {
    await productoBorrado.update({ borrado_logico: 1 });
    return { producto: productoBorrado, message: "Cliente eliminado correctamente" };
  }
  return null;
};

const editarProducto = async (id, producto) => {
  const producto_actual = await Producto.findByPk(id);
  if (producto_actual) {
    await producto_actual.update({
        nombre: producto.nombre,
        valor_venta_1: producto.valor_venta_1,
        valor_venta_2: producto.valor_venta_2,
        valor_venta_3: producto.valor_venta_3,
        costo: producto.costo,
        categoria_id: producto.categoria_id,
    });
  }

  return { producto: producto_actual, message: "Producto Editado correctamente" };;
};

module.exports = {
    mostrarCategoriaProducto,
    mostrarProducto,
    crearProducto,
    buscarProducto,
    borradoLogico,
    editarProducto

  
};
