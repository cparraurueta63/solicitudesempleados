const Cliente = require("../model/cliente");
const sequelize = require("../database/conexion");

const mostrarClientes = async () => {
  const clientes = await Cliente.findAll({
    where: {
      borrado_logico: 0,
    },
  });
  return clientes;
};
const crearCliente = async (cliente) => {
  const cliente_nuevo = await Cliente.create({
    nombre: cliente.nombre,
    cedula: cliente.cedula,
    celular: cliente.celular,
    correo: cliente.correo,
    direccion: cliente.direccion,
  });

  return cliente_nuevo;
};

const buscarCliente = async (nombre) => {
  const clienteFiltrado = await sequelize.query(
    `SELECT * FROM clientes WHERE nombre LIKE '%${nombre}%'`
  );

  return clienteFiltrado[0];
};

const borradoLogico = async (id) => {
  const clienteBorrado = await Cliente.findByPk(id);
  if (clienteBorrado) {
    await clienteBorrado.update({ borrado_logico: 1 });
    return { cliente: clienteBorrado, message: "Cliente eliminado correctamente" };

  
  }
  return null;
};

const editarCliente = async (id, cliente) => {
  const cliente_actual = await Cliente.findByPk(id);
  if (cliente_actual) {
    await cliente_actual.update({
      nombre: cliente.nombre,
      celular: cliente.celular,
      correo: cliente.correo,
      direccion: cliente.direccion,
    });
  }

  return { cliente: cliente_actual, message: "Cliente Editado correctamente" };;
};

module.exports = {
  mostrarClientes,
  crearCliente,
  buscarCliente,
  editarCliente,
  borradoLogico,
};
