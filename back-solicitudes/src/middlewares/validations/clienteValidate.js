const { check, validationResult } = require("express-validator");
// const { Empleado } = require("../../model/empleado");

const cliente = {};

empleado.validarCrearCliente = async (request, response, next) => {
  const validaciones = [
    check("nombre")
      .notEmpty()
      .withMessage("El nombre es requerido")
      .matches(/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/)
      .withMessage("Solo se permiten letras y espacios")
      .isLength({ max: 150, min: 2 })
      .withMessage("Entrada inválida")
      .customSanitizer((value) => {
        if (typeof value === "string") {
          return value.trim().replace(/\s+/g, " ").toUpperCase();
        }
        return value;
      }),
    check("cedula")
      .notEmpty()
      .withMessage("El nombre es requerido")
      .matches(/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/)
      .withMessage("Solo se permiten letras y espacios")
      .isLength({ max: 11, min: 7 })
      .withMessage("El tamaño de la cedula no es correcto")
      .customSanitizer((value) => {
        if (typeof value === "string") {
          return value.trim().replace(/\s+/g, " ").toUpperCase();
        }
        return value;
      }),

    check("celular")
      .notEmpty()
      .withMessage("El celular es requerido")
      .matches(/^3\d{9}$/)
      .withMessage("Celular inválido"),

    check("correo")
      .notEmpty()
      .withMessage("El correo electrónico es requerido")
      .isEmail()
      .withMessage("Correo electrónico inválido")
      .isLength({ max: 100 })
      .withMessage(
        "El correo electrónico debe tener como máximo 100 caracteres"
      ),
    check("direccion")
      .notEmpty()
      .withMessage("La direccion es obligatoria")
      .isLength({ max: 100 })
      .withMessage(
        "El correo electrónico debe tener como máximo 100 caracteres"
      )
      .isLength({ max: 500 })
      .withMessage("La descripción debe tener como máximo 500 caracteres")
      .matches(/^[a-zA-Z0-9\s\.,_-]+$/)
      .withMessage("La entrada contiene caracteres no permitidos"),
  ];

  await Promise.all(validaciones.map((validation) => validation.run(request)));

  const errores = validationResult(request);
  if (!errores.isEmpty()) {
    const respuesta = {
      status: "error",
      message: "Errores de validacion",
      hasErrors: true,
      errors: errores.array(),
    };
    return response.status(400).json(respuesta);
  }
  next();
};

empleado.validarEditarCliente = async (request, response, next) => {
  const validaciones = [
    check("nombre")
      .notEmpty()
      .withMessage("El nombre es requerido")
      .matches(/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/)
      .withMessage("Solo se permiten letras y espacios")
      .isLength({ max: 150, min: 2 })
      .withMessage("Entrada inválida")
      .customSanitizer((value) => {
        if (typeof value === "string") {
          return value.trim().replace(/\s+/g, " ").toUpperCase();
        }
        return value;
      }),

    check("celular")
      .notEmpty()
      .withMessage("El celular es requerido")
      .matches(/^3\d{9}$/)
      .withMessage("Celular inválido"),

    check("correo")
      .notEmpty()
      .withMessage("El correo electrónico es requerido")
      .isEmail()
      .withMessage("Correo electrónico inválido")
      .isLength({ max: 100 })
      .withMessage(
        "El correo electrónico debe tener como máximo 100 caracteres"
      ),
    check("direccion")
      .notEmpty()
      .withMessage("La direccion es obligatoria")
      .isLength({ max: 100 })
      .withMessage(
        "El correo electrónico debe tener como máximo 100 caracteres"
      )
      .isLength({ max: 500 })
      .withMessage("La descripción debe tener como máximo 500 caracteres")
      .matches(/^[a-zA-Z0-9\s\.,_-]+$/)
      .withMessage("La entrada contiene caracteres no permitidos"),
  ];

  await Promise.all(validaciones.map((validation) => validation.run(request)));

  const errores = validationResult(request);
  if (!errores.isEmpty()) {
    const respuesta = {
      status: "error",
      message: "Errores de validacion",
      hasErrors: true,
      errors: errores.array(),
    };
    return response.status(400).json(respuesta);
  }
  next();
};
module.exports = cliente;
