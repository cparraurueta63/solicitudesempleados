const { check, validationResult } = require("express-validator");
// const { Empleado } = require("../../model/empleado");

const empleado = {};

empleado.validarEmpleado = async (request, response, next) => {
  const validaciones = [

    check("nombre")
      .notEmpty()
      .withMessage("El nombre es requerido")
      .matches(/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/)
      .withMessage("Solo se permiten letras y espacios")
      .isLength({ max: 150, min: 2 })
      .withMessage("Entrada inválida")
      .customSanitizer((value) => {
        if (typeof value === "string") {
          return value.trim().replace(/\s+/g, " ").toUpperCase();
        }
        return value;
      }),

    check("fecha_ingreso")
      .notEmpty()
      .withMessage("La fecha de ingreso es obligatoria")
      .matches(/^\d{4}-\d{2}-\d{2}$/)
      .withMessage("La fecha de ingreso debe estar en el formato YYYY-MM-DD")
      .custom((value) => {
        const fechaIngresada = new Date(value);
        if (isNaN(fechaIngresada.getTime())) {
          throw new Error("La fecha de ingreso no es válida");
        }
        return true;
      }),

    check("salario")
      .notEmpty()
      .withMessage("El salario es requerido")
      .isNumeric()
      .withMessage("El salario debe ser un número")
      .isFloat({ min: 0 })
      .withMessage("El salario debe ser un número positivo")
      .custom((value) => {
        if (value < 0) {
          throw new Error("El salario no puede ser negativo");
        }
        return true;
      }),
  ];

  await Promise.all(validaciones.map((validation) => validation.run(request)));

  const errores = validationResult(request);
  if (!errores.isEmpty()) {
    const respuesta = {
      status: "error",
      message: "Errores de validacion",
      hasErrors: true,
      errors: errores.array(),
    };
    return response.status(400).json(respuesta);
  }
  next();
};


empleado.validarEditarEmpleado = async (request, response , next)=>{

  const validaciones = [

    check("nombre")
      .notEmpty()
      .withMessage("El nombre es requerido")
      .matches(/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/)
      .withMessage("Solo se permiten letras y espacios")
      .isLength({ max: 150, min: 2 })
      .withMessage("Entrada inválida")
      .customSanitizer((value) => {
        if (typeof value === "string") {
          return value.trim().replace(/\s+/g, " ").toUpperCase();
        }
        return value;
      }),

    // check("fecha_ingreso")
    //   .notEmpty()
    //   .withMessage("La fecha de ingreso es obligatoria")
    //   .matches(/^\d{4}-\d{2}-\d{2}$/)
    //   .withMessage("La fecha de ingreso debe estar en el formato YYYY-MM-DD")
    //   .custom((value) => {
    //     const fechaIngresada = new Date(value);
    //     if (isNaN(fechaIngresada.getTime())) {
    //       throw new Error("La fecha de ingreso no es válida");
    //     }
    //     return true;
    //   }),

    check("salario")
      .notEmpty()
      .withMessage("El salario es requerido")
      .isNumeric()
      .withMessage("El salario debe ser un número")
      .isFloat({ min: 0 })
      .withMessage("El salario debe ser un número positivo")
      .custom((value) => {
        if (value < 0) {
          throw new Error("El salario no puede ser negativo");
        }
        return true;
      }),
  ];

  await Promise.all(validaciones.map((validation) => validation.run(request)));

  const errores = validationResult(request);
  if (!errores.isEmpty()) {
    const respuesta = {
      status: "error",
      message: "Errores de validacion",
      hasErrors: true,
      errors: errores.array(),
    };
    return response.status(400).json(respuesta);
  }
  next();
}; 

module.exports = empleado;
