const { check, validationResult } = require("express-validator");
const  Empleado  = require("../../model/empleado");

const solicitudes = {};

solicitudes.crearSolicitudes = async (request, response, next) => {
  const validaciones = [
    check("codigo")
      .notEmpty()
      .withMessage("El código es requerido")
      .matches(/^\S+$/)
      .withMessage("No se permiten espacios"),

    check("descripcion")
      .notEmpty()
      .withMessage("La descripción es requerida")
      .matches(/^[a-zA-Z0-9_]+$/)
      .withMessage(
        "El campo solo puede contener letras, números y guiones bajos"
      ),

    check("resumen")
      .notEmpty()
      .withMessage("El resumen es requerido")
      .matches(/^[a-zA-Z0-9_]+$/)
      .withMessage(
        "El campo solo puede contener letras, números y guiones bajos"
      ),

    check("id_empleado")
      .notEmpty()
      .withMessage("El ID del empleado es requerido")
      .isInt()
      .withMessage("El ID del empleado debe ser un número entero")
      .custom(async (id_empleado) => {
        try {
          const empleado = await Empleado.findByPk(id_empleado);
          if (!empleado) {
            throw new Error("El empleado con el ID especificado no existe");
          }
        } catch (error) {
          throw new Error("Error al buscar el empleado en la base de datos");
        }
      })
      .withMessage("El empleado con el ID especificado no existe"),

      check("id_empleado").custom(async (id_empleado) => {
        const empleado = await Empleado.findByPk(id_empleado);
        if (!empleado) {
          throw new Error("El empleado con el ID especificado no existe");
        }
  
        const solicitudesActivas = await Solicitudes.count({
          where: {
            id_empleado: id_empleado
          }
        });
  
        if (solicitudesActivas > 0) {
          throw new Error("El empleado ya tiene una solicitud activa");
        }
      })




  ];

  await Promise.all(validaciones.map((validation) => validation.run(request)));

  const errores = validationResult(request);
  if (!errores.isEmpty()) {
    const respuesta = {
      status: "error",
      message: "Errores de validacion",
      hasErrors: true,
      errors: errores.array(),
    };
    return response.status(400).json(respuesta);
  }
  next();
};

module.exports = solicitudes;
