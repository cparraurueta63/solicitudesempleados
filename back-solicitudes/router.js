const express = require('express');
const router = express.Router();
//validadores
const empleadoValidate = require('./src/middlewares/validations/empleadoValidate');
const solicitudValidate = require('./src/middlewares/validations/solicitudValidate');
//controladores
const empleadoController = require('./src/controllers/empleadoController');
const solicitudesController = require('./src/controllers/solicitudesController');
const clienteController = require('./src/controllers/clienteController');
const productoController = require('./src/controllers/productoController');
const servicioController = require('./src/controllers/servicioController');
const gastoController = require('./src/controllers/gastosController');
const ingresosController = require('./src/controllers/ingresosController');

//rutas empleado 
router.get('/empleados', empleadoController.empleados);
router.get('/empleados/filtro', empleadoController.buscarEmpleado);
router.post('/empleados',  empleadoValidate.validarEmpleado, empleadoController.crearEmpleado);
router.put('/empleados/:id', empleadoValidate.validarEditarEmpleado, empleadoController.editarEmpleado);

//rutas solicitudes 

router.get('/solicitudes', solicitudesController.solicitudes);
router.post('/solicitudes', solicitudValidate.crearSolicitudes, solicitudesController.crearSolicitud);
router.delete('/solicitudes/:id', solicitudesController.eliminarSolicitud);

//rutas cliente 
router.get('/clientes', clienteController.obtenerClientes);
router.post('/clientes', clienteController.crearCliente);
router.get('/clientes/filtro', clienteController.buscarCliente);
router.put('/clientes/borrar/:id', clienteController.eliminarCliente);
router.put('/clientes/:id', clienteController.editarCliente);

//rutas producto 
router.get('/productos', productoController.obtenerproductos);
router.get('/productos/categorias', productoController.obtenerproductosCategorias);
router.post('/productos', productoController.crearProducto);
router.get('/productos/filtro', productoController.buscarProducto);
router.put('/productos/borrar/:id', productoController.eliminarProducto);
router.put('/productos/:id', productoController.editarProducto);


//rutas servicios 
router.post('/servicios', servicioController.crearServicio);
router.get('/servicios', servicioController.obtenerServicios);
router.get('/servicios/filtro', servicioController.buscarServicio);
router.put('/servicios/borrar/:id', servicioController.eliminarServicio);
router.put('/servicios/:id', servicioController.editarServicio);

//rutas gastos 
router.post('/gastos', gastoController.crearGasto);
router.get('/gastos', gastoController.obtenerGastos);
router.get('/gastos/filtro', gastoController.buscarGasto);
router.put('/gastos/borrar/:id', gastoController.eliminarGasto);
router.put('/gastos/:id', gastoController.editarGasto);

//rutas ingresos 
router.post('/ingresos', ingresosController.crearIngreso);
router.get('/ingresos', ingresosController.obteneringresos);
router.get('/ingresos/filtro', ingresosController.buscarIngreso);
router.put('/ingresos/borrar/:id', ingresosController.eliminarIngreso);
router.put('/ingresos/:id', ingresosController.editarIngreso);




module.exports = router;