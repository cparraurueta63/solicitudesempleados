const express = require("express");
const routes = require("./router");
const db = require("./src/database/conexion");
const app = express();
const port = process.env.Port || 8005;
const bodyParser = require("body-parser");
const cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "DELETE", "PUT"],
  })
);

db.authenticate()
  .then(async () => {
    console.log("Conexión a la base de datos establecida correctamente.");   
    app.listen(port, () =>
      console.log("Servidor corriendo en el puerto " + port)
    );
  })
  .catch((error) => {
    console.error("Error al conectarse a la base de datos:", error);
  });


app.use("/", routes);
